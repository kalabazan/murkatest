﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;


public class GameController : MonoBehaviour {
    public LineRenderer LineRenderer;
    /// <summary>
    /// Кол-во секунд на рисование первой фигуры
    /// </summary>
    public float TimeToDraw = 15;
    public float TimeLeft;
    public int Score = 0;

    /// <summary>
    /// Кэш загруженых из файла шаблонов
    /// </summary>
    private List<Template> _templates;
    /// <summary>
    /// Текущий выбранный шаблон
    /// </summary>
    private Template _curTemplate;
    /// <summary>
    /// Кэш вершин в мировых координатах
    /// </summary>
    private Vector2[] _worldPoints;
    private bool _drawingStarted = false;
    private bool[] _completedPoints;
    private int _lastCompletedPoint =-1;

    public delegate void FailHendller();
    public event FailHendller OnFail;
    public delegate void CompleteHendller();
    public event CompleteHendller OnComplete;

    [HideInInspector]
    public Vector2[] _nearPoints = new Vector2[2];

    public static GameController Instance;

    void Awake() {
        Instance = this;
    }

    void Start() {
        TimeLeft = TimeToDraw;
        OnFail += () => {
            _drawingStarted = false;
            LineRenderer.enabled = false;
            Debug.Log("Fail");
        };
        OnComplete += () => {
            _drawingStarted = false;
            Debug.Log("Complete");
            StartGame();
            Score++;
            GUIController.Instance.ScoreLabel.text = Score.ToString();
        };
    }

    // Спавнит фигуры по случайному шаблону из сохраненных
    public void StartGame() {
        LineRenderer.enabled = true;
        _templates = LoadData();
        _curTemplate = _templates[Random.Range(0, _templates.Count)];
        TimeLeft = TimeToDraw - Score;

        _lastCompletedPoint = -1;

        var tempList = new List<Vector2>();
        // ПРоход по точкам, преобразование в мировые координаты и вычисление промежуточных точек
        for(int i = 0; i < _curTemplate.Points.Count; i++) {
            //var x = (_curTemplate.Points[i].X - _texture.width / 2f) / _texture.width * 9f;
            //var y = (_curTemplate.Points[i].Y - _texture.height / 2f) / _texture.height * 9f;/
            // Каст координат из текстурных в мировые
            var x = (_curTemplate.Points[i].X - 128) / 256 * 9f;
            var y = (_curTemplate.Points[i].Y - 128) / 256 * 9f;

            var tmpPos = new Vector2(x, y);
            tempList.Add(tmpPos);
            if(i + 1 == _curTemplate.Points.Count) {
                continue;
            }
            x = (_curTemplate.Points[i + 1].X - 128) / 256 * 9f;
            y = (_curTemplate.Points[i + 1].Y - 128) / 256 * 9f;
            var nextPos = new Vector2(x, y);
            var dist = Vector2.Distance(tmpPos, nextPos);
            var counter = 0;
            // нахождение промежуточных точек
            while(counter < 2) {
                tmpPos = Utils.NextPointOnLine(tmpPos, nextPos, 1f);
                tempList.Add(tmpPos);
                dist = Vector2.Distance(tmpPos, nextPos);
                counter++;
            }


        }

        _worldPoints = new Vector2[tempList.Count];
        _worldPoints = tempList.ToArray();

        _completedPoints = new bool[_worldPoints.Length + 1];// в первую точку нужно попасть дважды
        LineRenderer.SetVertexCount(_worldPoints.Length + 1);

        for(int i = 0; i < _worldPoints.Length; i++) {
            LineRenderer.SetPosition(i, _worldPoints[i]);
        }
        LineRenderer.SetPosition(_worldPoints.Length, _worldPoints[0]); // из последней точки в первую


        _drawingStarted = false;

        StartCoroutine(StartCountDown());
        StartCoroutine(CountDown());
    }

    void OnDrawGizmos() {
        if(_worldPoints == null) {
            return;
        }
        for(int i = 0; i < _worldPoints.Length; i++) {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(_nearPoints[0], UserInput.Instance.TouchPosition);
            Gizmos.DrawLine(_nearPoints[1], UserInput.Instance.TouchPosition);
            Gizmos.DrawLine(_nearPoints[1], _nearPoints[0]);

            Gizmos.color = _completedPoints[i] ? Color.green : Color.blue;
            Gizmos.DrawSphere(_worldPoints[i], 0.2f);
        }
    }

    void Update() {
        if(_worldPoints == null || !_drawingStarted || !UserInput.Instance.ButtonPressed) {
            return;
        }

        float dist = Mathf.Infinity;
        //прбегаем по всем вершинам
        for(int i = 0; i < _worldPoints.Length; i++) {
            var secIndex = 0;
            if(i + 1 == _worldPoints.Length) {// после последней вершины снова первая
                secIndex = 0;
            } else {
                secIndex = i + 1;
            }
            // вычисляем расстояние от отрезка между двумя вершинами до точки тача
            var d = Vector2.Distance(UserInput.Instance.TouchPosition, _worldPoints[i]);//Utils.PointToLineDist(UserInput.Instance.TouchPosition, _worldPoints[i], _worldPoints[secIndex]);
            // елси оно меньше предыдущего, запоминаем точки
            //Debug.Log("dist = " + dist);
            //Debug.Log("d = " + d);
            if(d < dist) {
                _nearPoints[0] = _worldPoints[i];
                if(i + 1 == _worldPoints.Length) {// после последней вершины снова первая
                    _nearPoints[1] = _worldPoints[0];
                } else {
                    _nearPoints[1] = _worldPoints[secIndex];
                }
                dist = d;
            }
            if(Vector2.Distance(UserInput.Instance.TouchPosition, _worldPoints[i]) < 1f) {
                _completedPoints[i] = true;
            }
            if(_completedPoints[_completedPoints.Length - 2] && Vector2.Distance(UserInput.Instance.TouchPosition, _worldPoints[0]) < 1f) {
                _completedPoints[_completedPoints.Length - 1] = true;
            }
        }

        // Если расcтояние до ближайшей грани больше допустимого или отпустили кнопку(палец), фейлим
        if(dist > 2f || !UserInput.Instance.ButtonPressed) {
            OnFail();
        }

        bool win = true;
        for(int i = 0; i < _completedPoints.Length; i++) {
            win &= _completedPoints[i];
        }
        if(win) {
            OnComplete();
        }

    }

    /// <summary>
    /// Отсчет времени на рисование фигуры
    /// </summary>
    IEnumerator CountDown() {
        GUIController.Instance.TimeSlider.maxValue = TimeLeft;
        GUIController.Instance.TimeSlider.value = TimeLeft;
        while(!_drawingStarted) {
            yield return null;
        }
        while(_drawingStarted) {
            Debug.Log("CountDown Tick. Time left =" + TimeLeft);
            yield return new WaitForSeconds(1);
            TimeLeft--;
            GUIController.Instance.TimeSlider.value = TimeLeft;
            if(TimeLeft <= 0) {
                OnFail();
            }
        }
    }

    /// <summary>
    /// Стартовый отсчет
    /// </summary>
    IEnumerator StartCountDown() {
        var cd = 3;
        GUIController.Instance.CdLabel.enabled = true;
        GUIController.Instance.CdLabel.text = cd.ToString();
        while(!_drawingStarted) {
            yield return new WaitForSeconds(1);
            Debug.Log("Start CountDown Tick. cd left =" + cd);
            cd--;
            GUIController.Instance.CdLabel.text = cd.ToString();
            if(cd <= 0) {
                GUIController.Instance.CdLabel.enabled = false;
                UserInput.Instance.TouchPosition = _worldPoints[0];
                _drawingStarted = true;
            }
        }
    }

    /// <summary>
    /// Читает данные из файла с шаблонами
    /// </summary>
    /// <returns></returns>
    public static List<Template> LoadData() {
        var binaryFormatter = new BinaryFormatter();
        List<Template> retList = null;
        FileStream fs = new FileStream(Application.streamingAssetsPath + "/Templates.txt", FileMode.OpenOrCreate, FileAccess.Read);
        if(fs.Length > 0) {
            retList = (List<Template>)binaryFormatter.Deserialize(fs);
        }
        fs.Close();
        if(retList.Count == 0) {
            retList.Add(new Template());
            retList[0].Name = "New Template";
            retList[0].Points.Add(new Vector2s());
        }
        return retList;
    }


}
