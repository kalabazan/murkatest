﻿using UnityEngine;
using System.Collections;

public class UserInput : MonoBehaviour {
    public Transform Trail;
    public Vector2 TouchPosition;
    /// <summary>
    /// Кнопка зажата(палец на экране)
    /// </summary>
    public bool ButtonPressed;

    static public UserInput Instance;

    void Awake() {
        Instance = this;
    }

    void Update() {
        ButtonPressed = true;
        if(Input.GetMouseButton(0)) {
            var pos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            TouchPosition = pos;
            Trail.position = new Vector3(pos.x, pos.y, -5);
        } else if(Input.touchCount > 0) {
            var pos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
            TouchPosition = pos;
            Trail.position = new Vector3(pos.x, pos.y, -5);
        } else {
            ButtonPressed = false;
        }
    }
}
