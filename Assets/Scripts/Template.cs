﻿using System;
using UnityEngine;
using System.Collections.Generic;




[Serializable]
public class Template {
    public string Name = "New Template";
    public List<Vector2s> Points = new List<Vector2s>();

}
