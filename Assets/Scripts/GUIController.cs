﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GUIController : MonoBehaviour {

    public GameObject InGameUI;
    public GameObject MenuUI;

    public Slider TimeSlider;
    public Text CdLabel;
    public Text ScoreLabel;

    public static GUIController Instance;
    void Awake() {
        Instance = this;
    }

    void Start() {
        GameController.Instance.OnFail += () => {
            InGameUI.SetActive(false);
            MenuUI.SetActive(true);
        };

        InGameUI.SetActive(false);
        MenuUI.SetActive(true);
    }

    public void StartGameButton() {
        InGameUI.SetActive(true);
        MenuUI.SetActive(false);
        GameController.Instance.StartGame();
    }

}
