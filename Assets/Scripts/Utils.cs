﻿using UnityEngine;
using System;

/// <summary>
/// Сериализуемый Vector2
/// </summary>
[Serializable]
public class Vector2s {
    public float X;
    public float Y;

    public Vector2s(float x, float y) {
        X = x;
        Y = y;
    }
    public Vector2s(Vector2 vector2) {
        X = vector2.x;
        Y = vector2.y;
    }
    public Vector2s(Vector2s vector2s) {
        X = vector2s.X;
        Y = vector2s.Y;
    }
    public Vector2s() {
        X = 0f;
        Y = 0f;
    }
}

public class Utils : MonoBehaviour {

    /// <summary>
    /// Рисует точку(квадрат?) на текстуре
    /// </summary>
    static public void DrawPoint(Texture2D texture, Vector2s pos, Color color) {
        var r = 5;
        for(int i = (int)pos.X - r; i < pos.X + r; i++) {
            for(int j = (int)pos.Y - r; j < pos.Y + r; j++) {
                texture.SetPixel(i, j, color);
            }
        }
        texture.Apply();
    }

    /// <summary>
    /// Рисует отрезок на текстуре между заданными точками
    /// </summary>
    static public void DrawLine(Texture2D texture, Vector2 point1, Vector2 point2, Color color) {
        Vector2 curPoint = point1;
        Vector2 dir = point2 - point1;
        dir.Normalize();

        var prog = 0;
        while((Mathf.Abs(curPoint.x - point2.x) > 2) || (Mathf.Abs(curPoint.y - point2.y) > 2)) {
            var x = dir.x * prog + point1.x;
            var y = dir.y * prog + point1.y;

            texture.SetPixel((int)x, (int)y, color);
            curPoint.Set(x, y);

            prog++;
        }
        texture.Apply();
    }

    /// <summary>
    /// Вычисляет следующую точку на отрезке заданом двумя точками
    /// </summary>
    static public Vector2 NextPointOnLine(Vector2 point1, Vector2 point2, float step) {
        Vector2 dir = point2 - point1;
        dir.Normalize();

        var x = dir.x * step + point1.x;
        var y = dir.y * step + point1.y;

        return new Vector2(x, y);
    }

    /// <summary>
    /// Расстояние от точки тод прямой заданной двумя точками
    /// </summary>
    static public float PointToLineDist(Vector2 point, Vector2 linePoint1, Vector2 linePoint2) {
        var x = point.x;
        var y = point.y;
        var x0 = linePoint1.x;
        var y0 = linePoint1.y;
        var x1 = linePoint2.x;
        var y1 = linePoint2.y;
        var sqrt = (x1 - x0) * (x1 - x0) + (y1 - y0) * (x1 - x0);
        return Mathf.Abs(Mathf.Abs(((y0 - y1) * x + (x1 - x0) * y + (x0 * y1 - x1 * y0))) / Mathf.Sqrt(sqrt));
    }


    static public Vector2s ScreenToTexturePoint(Vector2s point, Rect textureRect) {
        var ret = new Vector2s(point);
        ret.X -= textureRect.x;
        ret.Y -= textureRect.y;
        ret.Y = ret.Y * -1 + textureRect.width;
        return ret;
    }

}
