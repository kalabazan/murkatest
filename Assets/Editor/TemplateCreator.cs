﻿/// Текущая точка помечена <============== и отрисовывается красным цветом (остальные - синим)
/// Текущую точку можно перетаскивать мышкой
/// Порядок точек имеет значение, грани отрисовуются поочередно, грань от последней вершины до первой отрисовывается автоматом
/// Данные хранятся в файле "/Resources/Templates.txt" в бинарном виде
/// Данные сохраняются при нажатии на кнопку "Save" и при закрытии окна
/// В текстовое поле над кнопкой "Save" можно ввести название шаблона, название просто для удобства


using UnityEngine;
using System.Collections.Generic;
using UnityEditor;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

public class TemplateCreator : EditorWindow {
    /// <summary>
    /// Текстура на которой ресуется превью
    /// </summary>
    private Texture2D _texture = new Texture2D(256, 256);

    private List<Template> _templates = new List<Template>();
    /// <summary>
    /// Хранит индекс выбранного шаблона
    /// </summary>
    private int _seleсtedTemplateIndex;
    /// <summary>
    /// Хранит индекс выбранной точки шаблона
    /// </summary>
    private int _seleсtedPointIndex;

    private Color LightGray = new Color32(150, 150, 150, 255);
    private static EditorWindow _window;

    [MenuItem("Tools/Template Creator")]
    public static void ShowWindow() {
        _window = GetWindow(typeof(TemplateCreator));
        _window.minSize = new Vector2(800, 450);
        _window.titleContent = new GUIContent("Template Creator");
    }

    public void Awake() {
        _texture = new Texture2D(256, 256);

        var bf = new BinaryFormatter();
        FileStream fs = new FileStream(Application.streamingAssetsPath + "/Templates.txt", FileMode.OpenOrCreate, FileAccess.Read);
        if(fs.Length > 0) {
            _templates = (List<Template>)bf.Deserialize(fs);
        }
        fs.Close();
        if(_templates.Count == 0) {
            _templates.Add(new Template());
            _templates[0].Name = "New Template";
            _templates[0].Points.Add(new Vector2s());
        }

    }



    Vector2 _scroll = Vector2.zero;
    Vector2 _scroll1 = Vector2.zero;
    private Rect _textureRect;
    private bool _dragPointFlag;
    private Rect _rect;
    private void OnGUI() {
        if(_templates == null || _templates.Count == 0 || _templates[_seleсtedTemplateIndex] == null || _templates[_seleсtedTemplateIndex].Points == null || _texture == null)
            return;

        MouseInput();

        EditorGUILayout.BeginHorizontal(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true));

        #region LeftPanel
        var textureRectB = EditorGUILayout.BeginVertical(GUILayout.ExpandWidth(false), GUILayout.Width(270));
        _textureRect = new Rect(textureRectB.x + 14, textureRectB.y + 15, _texture.width, _texture.height);
        EditorGUI.DrawPreviewTexture(_textureRect, _texture);
        GUILayout.Space(300);
        _templates[_seleсtedTemplateIndex].Name = GUILayout.TextField(_templates[_seleсtedTemplateIndex].Name);
        GUILayout.Space(10);
        if(GUILayout.Button("Save")) {
            SaveButtonClick();
        }
        EditorGUILayout.EndVertical();
        #endregion

        #region CentrPanel(Points)
        _rect = EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true), GUILayout.MinWidth(250));
        GUILayout.Space(10);
        if(GUILayout.Button("Add point")) {
            _templates[_seleсtedTemplateIndex].Points.Add(new Vector2s());
            _seleсtedPointIndex = _templates[_seleсtedTemplateIndex].Points.Count - 1;
        }
        GUILayout.Space(10);
        _scroll = GUILayout.BeginScrollView(_scroll, false, true);
        DrawPointsList();
        GUILayout.EndScrollView();
        GUILayout.Space(5);
        EditorGUILayout.EndVertical();
        #endregion

        GUILayout.Space(15);

        #region RightPanel(Templates)
        var rect1 = EditorGUILayout.BeginVertical(GUILayout.ExpandHeight(true), GUILayout.ExpandWidth(true), GUILayout.MinWidth(250));
        GUILayout.Space(10);
        if(GUILayout.Button("Add template")) {
            var t = new Template();
            t.Points = new List<Vector2s>();
            _templates.Add(t);
            _seleсtedTemplateIndex = _templates.Count - 1;
        }
        GUILayout.Space(10);
        _scroll1 = EditorGUILayout.BeginScrollView(_scroll1, false, true, GUILayout.ExpandWidth(true), GUILayout.ExpandHeight(true));
        DrawTemplatesList();
        EditorGUILayout.EndScrollView();
        GUILayout.Space(5);
        EditorGUILayout.EndVertical();
        #endregion

        EditorGUILayout.EndHorizontal();

        #region DrawPreview
        _texture = new Texture2D(256, 256);
        _texture.wrapMode = TextureWrapMode.Clamp;
        if(_templates[_seleсtedTemplateIndex].Points.Count == 0) {
            return;
        }
        // Пробегаем по всем точкам и рисуем между ними линии
        for(int i = 0; i < _templates[_seleсtedTemplateIndex].Points.Count; i++) {
            var p1 = new Vector2(_templates[_seleсtedTemplateIndex].Points[i].X, _templates[_seleсtedTemplateIndex].Points[i].Y);
            Vector2 p2;
            if(i == _templates[_seleсtedTemplateIndex].Points.Count - 1) {
                p2 = new Vector2(_templates[_seleсtedTemplateIndex].Points[0].X, _templates[_seleсtedTemplateIndex].Points[0].Y); // от последней точки к первой
            } else {
                p2 = new Vector2(_templates[_seleсtedTemplateIndex].Points[i + 1].X, _templates[_seleсtedTemplateIndex].Points[i + 1].Y);
            }
            Utils.DrawLine(_texture, p1, p2, Color.black);
        }
        // Помечаем каждую вершину
        for(int i = 0; i < _templates[_seleсtedTemplateIndex].Points.Count; i++) {
            Utils.DrawPoint(_texture, _templates[_seleсtedTemplateIndex].Points[i], i == _seleсtedPointIndex ? Color.red : Color.blue);
        }
        #endregion
    }

    /// <summary>
    /// Обработка инпута с мыши, таскание точeк
    /// </summary>
    void MouseInput() {
        if(Event.current.type == EventType.MouseDrag && _textureRect.Contains(Event.current.mousePosition)) {
            _dragPointFlag = true;
            var tmpVec = Utils.ScreenToTexturePoint(new Vector2s(Event.current.mousePosition), _textureRect);
            _templates[_seleсtedTemplateIndex].Points[_seleсtedPointIndex] = tmpVec;
            _dragPointFlag = false;
            _window.Repaint();
        }
    }



    void DrawTemplatesList() {
        for(int i = 0; i < _templates.Count; i++) {
            Rect rectt = EditorGUILayout.BeginVertical();
            EditorGUI.DrawRect(new Rect(rectt.x, rectt.y, rectt.width, rectt.height), LightGray);

            GUILayout.Space(10);
            GUILayout.BeginHorizontal();
            GUILayout.Space(10);
            GUILayout.Label(string.Concat(_templates[i].Name, "      points count: ", _templates[i].Points.Count));
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal(GUILayout.Height(20), GUILayout.ExpandWidth(true));
            GUILayout.Space(10);
            if(GUILayout.Button("Select")) {
                _seleсtedTemplateIndex = i;
            }
            if(GUILayout.Button("Delete")) {
                if(EditorUtility.DisplayDialog("Delete selected Template", "You cannnot undo this action!", "Ok")) {
                    _seleсtedTemplateIndex = 0;
                    _templates.RemoveAt(i);
                }
            }
            GUILayout.Space(20);
            EditorGUILayout.EndHorizontal();
            EditorGUILayout.EndVertical();

        }

    }

    void DrawPointsList() {
        for(int i = 0; i < _templates[_seleсtedTemplateIndex].Points.Count; i++) {
            if(_templates[_seleсtedTemplateIndex].Points[_seleсtedPointIndex] == null) {
                continue;
            }

            Rect rectt = EditorGUILayout.BeginVertical();

            EditorGUI.DrawRect(new Rect(rectt.x, rectt.y, rectt.width, rectt.height), LightGray);

            EditorGUI.LabelField(new Rect(rectt.x + 5, rectt.y + 2, 300, 20), "Point #" + i + (i == _seleсtedPointIndex ? "       <=============" : ""));
            _templates[_seleсtedTemplateIndex].Points[i].X = Mathf.Clamp(EditorGUI.FloatField(new Rect(rectt.x + 5, rectt.y + 20, rectt.width / 2 - 6, 20), _templates[_seleсtedTemplateIndex].Points[i].X), 0, _texture.width);
            _templates[_seleсtedTemplateIndex].Points[i].Y = Mathf.Clamp(EditorGUI.FloatField(new Rect(rectt.x + 5 + rectt.width / 2, rectt.y + 20, rectt.width / 2 - 6, 20), _templates[_seleсtedTemplateIndex].Points[i].Y), 0, _texture.height);
            GUILayout.Space(50);
            EditorGUILayout.BeginHorizontal(GUILayout.Height(20), GUILayout.ExpandWidth(true));
            GUILayout.Space(5);
            if(GUILayout.Button("Select")) {
                _seleсtedPointIndex = i;
            }
            if(GUILayout.Button("Delete")) {
                _seleсtedPointIndex = _seleсtedPointIndex == i ? 0 : _seleсtedPointIndex;
                _templates[_seleсtedTemplateIndex].Points.RemoveAt(i);
            }
            GUILayout.Space(10);
            if(GUILayout.Button("Up")) {
                var newIndex = Mathf.Clamp(i - 1, 0, _templates[_seleсtedTemplateIndex].Points.Count - 1);
                var point = _templates[_seleсtedTemplateIndex].Points[_seleсtedPointIndex];
                _templates[_seleсtedTemplateIndex].Points.Remove(point);
                _templates[_seleсtedTemplateIndex].Points.Insert(newIndex, point);
                _seleсtedPointIndex = newIndex;
            }
            if(GUILayout.Button("down")) {
                var newIndex = Mathf.Clamp(i + 1, 0, _templates[_seleсtedTemplateIndex].Points.Count - 1);
                var point = _templates[_seleсtedTemplateIndex].Points[_seleсtedPointIndex];
                _templates[_seleсtedTemplateIndex].Points.Remove(point);
                _templates[_seleсtedTemplateIndex].Points.Insert(newIndex, point);
                _seleсtedPointIndex = newIndex;
            }
            GUILayout.EndHorizontal();

            EditorGUILayout.EndVertical();
        }
    }

    void SaveButtonClick() {
        SaveData();
    }

    /// <summary>
    /// Сохраняет данные в файл
    /// </summary>
    private void SaveData() {
        FileStream fs = new FileStream(Application.streamingAssetsPath + "/Templates.txt", FileMode.OpenOrCreate, FileAccess.Write);
        var bf = new BinaryFormatter();
        bf.Serialize(fs, _templates);
        fs.Close();
        Debug.Log("Data serialized!");
    }


    void OnDestroy() {
        SaveData();
    }


}
